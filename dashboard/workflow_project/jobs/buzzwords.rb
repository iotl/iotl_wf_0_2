hrows = [
  { cols: [ {class: 'cntre',value: 'Document No.'}, {class: 'adjpx',value: 'Pending With'}, {class: 'ljpx',value: 'Pending Since'} ] }
]

rows = [
  { cols: [ {class: 'left',value: 'IOTL/BD/LandProcurement/2017/0001'}, {class: 'adjpx',value: 'Sunil Agarwal'}, {class: 'ljpx',value: '1 day'}]},
  { cols: [ {class: 'left',value: 'IOTL/BD/LandProcurement/2017/0002'}, {class: 'adjpx',value: 'Sunil Agarwal'}, {class: 'ljpx',value: '2 days'}]},
  { cols: [ {class: 'left',value: 'IOTL/BD/LandProcurement/2017/0003'}, {class: 'adjpx',value: 'Arun Menon'}, {class: 'ljpx',value: '4 days'}]},
  { cols: [ {class: 'left',value: 'IOTL/BD/Railways/2017/0001'}, {class: 'adjpx',value: 'Arun Menon'}, {class: 'ljpx',value: '4 days'} ]},
  { cols: [ {class: 'left',value: 'IOTL/BD/Railways/2017/0002'}, {class: 'adjpx',value: 'Sunil Agarwal'}, {class: 'ljpx',value: '7 days'} ]},
  { cols: [ {class: 'left',value: 'IOTL/BD/Railways/2017/0003'}, {class: 'adjpx',value: 'Arun Menon'}, {class: 'ljpx',value: '7 days'} ]},
  { cols: [ {class: 'left',value: 'IOTL/BD/BusinessCase/2017/0001'}, {class: 'adjpx',value: 'Arun Menon'}, {class: 'ljpx',value: '10 days'}]},
  { cols: [ {class: 'left',value: 'IOTL/BD/BusinessCase/2017/0002'}, {class: 'adjpx',value: 'Sushanto Ghosh'}, {class: 'ljpx',value: '10 days'}]},
  { cols: [ {class: 'left',value: 'IOTL/BD/BusinessCase/2017/0003'}, {class: 'adjpx',value: 'Sushanto Ghosh'}, {class: 'ljpx',value: '10 days'}]},
  { cols: [ {class: 'left',value: 'IOTL/BD/Commissioning/2017/0001'}, {class: 'adjpx',value: 'Sushanto Ghosh'}, {class: 'ljpx',value: '12 days'} ]},
  { cols: [ {class: 'left',value: 'IOTL/BD/Commissioning/2017/0002'}, {class: 'adjpx',value: 'Sunil Agarwal'}, {class: 'ljpx',value: '12 days'} ]}
  
]

send_event('my-table', { hrows: hrows, rows: rows } )