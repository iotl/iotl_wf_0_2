# Populate the graph with some random points
points1 = []
points2 = []
(1..10).each do |i|
  points1 << { x: i, y: rand(50) }     # graph 1 initialization
  points2 << { x: i, y: rand(50) }     # graph 2 initialization
end
last_x = points1.last[:x]

SCHEDULER.every '2s' do
  points1.shift
  points2.shift
  last_x += 1
  points1 << { x: last_x, y: rand(50) }         # this is where you'd add a data element for graph 1
  points2 << { x: last_x, y: rand(50) }         # this is where you'd add a data element for graph 2
  send_event('convergence', points: [points1, points2])
end